<%@ Language=VBScript %>
<% 
option explicit 
Response.Expires = -1
response.buffer = false
Server.ScriptTimeout = 600
' All communication must be in UTF-8, including the response back from the request
Session.CodePage  = 65001
%>
<!-- #include file="lib/page_initialise.asp" -->
<!-- #include file="lib/incAspUpload.asp" -->
<%
Const UTF8_BOM = "ï»¿"

' Fucken option explicit!
Dim action, folder, fold, fil, redirectTo, wrap, archived, courseid, mappedFolder
Dim objStream, path, contentStr
Dim crsc,c,j,k,l,text, bool

Dim lessScript : lessScript = "		<script type=""text/javascript"" src=""../Layout/js/less-1.3.3.min.js""></script>"
Dim lessDevScript : lessDevScript = "		<script>less = {env: ""development"", poll: 5001}</script>" & vbNewLine &_
									"		<script type=""text/javascript"" src=""../Layout/js/less-1.3.3.min.js""></script>" & vbNewLine &_
									"		<script>less.watch();</script>"
Dim oCourse
Dim oTodo, aTodo
Dim aItems, oItem, oTemp
dim layoutsTemplate
Dim smfLoop, smfFile, smfDoc
Dim scormVersion
Dim i, filename, xpath, value, valuetype
Dim courseXml, settingsXml
Dim sJSON, oJSON, colour
Dim CourseRS, TodoRS, HistoryRS
Dim tempRS, tempJSON
Dim courseFrom, courseTo
Dim sShim


Dim filData, filName, xmlData, tEngine, zipPath, sourcePath, zResult, arrActions, intSize, vPath, aPath, data, res, href, bUpdated
Dim wrapScoVersion, selectedRuntime, contentPath, copyLess, cloneName, newName, strOutput, tempStr1, tempStr2, copyLayout, copyConfig

Dim myEmail, theirEmail, containerName, containerPath
Dim ncId, ncVals, ncSettings, sql, tix


db.openDefault()

redirectTo = "/"
archived = (request.querystring("archive") = "true")
action = request.querystring("action")
folder = Trim(request.querystring("folder"))

courseid = 0
If IsNumeric(request.querystring("id")) Then
	courseid = cint(request.querystring("id"))
End If

wrap = (request.querystring("wrap") > "")

If folder > "" then mappedFolder = Server.MapPath(folder)
if courseid > 0 then
	folder = GetCoursePath(courseid)
	mappedfolder = Server.MapPath(folder)
	' folder = db.getScalar("select folder from courses where id=" & courseid,"")
end if


select case lcase(action)

	case "ajax_setstage"
		Dim ss_stage : ss_stage = trim("" & request("stage"))
		call SetCourseStage(courseid, ss_stage)
		response.write "<span class='label" & GetLabelFromStage(ss_stage) & "'>" & Capitalise(ss_stage) & "</span>"
		endpage("")

	case "ajax-closedbugs"
		endpage("")

	case "ajax_pastedimage"
	    Set filData = New FreeASPUpload ' cus we are dealing with multipart/form-data
	    Call filData.Prepare() ' hack to populate form object
	    vPath = Trim(filData.form("path"))
	    If vPath = "" Then vPath = "/buggr/attachments"
		Response.Write SaveBase64ToFile (filData.form("base64"), filData.form("extn"), vPath)
	    set filData = Nothing
		EndPage("")




	case "post-delete-course-from-db"
		Call db.exec("DELETE FROM courses WHERE id={0}", courseid)
		EndPage("/engine/pages/index/")


	case "post-profile-setpassword"
		if Trim(request("newPassword")) > "" Then
			Call db.exec("UPDATE plebs SET password={0} WHERE id={1}", Array(md5er.hash(request("newPassword")), MyUserId))
		End If
		EndPage("/engine/pages/profile/")
	
	case "post-profile-setemail"
		if Trim(request("newEmail")) > "" Then
			Call db.exec("UPDATE plebs SET email={0} WHERE id={1}", Array(request("newEmail"), MyUserId))
		End If
		EndPage("/engine/pages/profile/")




	case "post_maketicketreplyitsownbug"
		tix = Buggr.MakeTicketReplyItsOwnBug(request.querystring("item"), MyUserName)
		EndPage("/engine/pages/tickets/?id=" & tix)
		
	case "post_conversation"
		tix = request.form("ticketid")
		Call Buggr.ReplyToTicket(tix, Trim(request.form("details")), Trim(request.form("url")), (request.form("done") = "y"), (request.form("notify") = "y"), MyUserName)
		EndPage("/engine/pages/tickets/?id=" & tix)

	case "ajax_submitbug"
		tix = Buggr.SubmitBug(Trim(request.form("text")), trim(request.form("level")), trim(request.form("screenshot")), MyUserName)
		endpage("")

	case "ajax_buglist"
		response.contentType = "application/json"
		Response.Write Buggr.GetBugListJson("open")
		EndPage("")

	case "ajax_bugdetails"
		response.contentType = "application/json"
		Response.Write Buggr.GetBugDetails(Request("TicketId"))
		EndPage("")







	case "htmlencode"
		response.write "<pre>"
		response.write server.htmlencode(IO.GetFileString(server.mappath(request("file")),""))
		response.write "</pre>"
		endpage("")






	case "magnet_randomise"
		Call db.connection.execute("UPDATE words SET x=(abs(random()) % 1100)+50,  y=(abs(random()) % 850)+50", adExecuteNoRecords)
		endpage("")

	case "magnet_move"
		i = clng(int(request("id")))
		j = clng(int(request("x")))
		k = clng(int(request("y")))
		l = clng(int(request("z")))
		Call db.connection.execute("UPDATE words SET x=" & db.SQLSafe(j) & ", y=" & db.SQLSafe(k) & ", z=" & db.SQLSafe(l) & " WHERE id=" & db.SQLSafe(i), adExecuteNoRecords)
		endpage("")

	case "magnet_ping"
		set tempRS = db.getrs("select * from words", empty)
		aItems = Array()
		If Not (tempRS.BOF AND tempRS.EOF) Then
			Do While Not tempRS.EOF
				Set oJSON = JSON.parse("{}")
				oJSON.set "id", clng(tempRS("id"))
				oJSON.set "x", clng(tempRS("x"))
				oJSON.set "y", clng(tempRS("y"))
				oJSON.set "z", clng(tempRS("z"))
				oJSON.set "word", trim(tempRS("word"))
				Call Push2Array(aItems, JSON.stringify(oJSON))
				tempRS.moveNext
			Loop
		End If
		response.contentType = "application/json"
		Response.Write "[" & Join(aItems,",") & "]"
		endpage("")

	case "magnet_add"
		data = split(trim(request("word")), " ")
		for i = 0 to ubound(data)
			text = data(i)
			Randomize Timer
			j = Int( ( 900 - 10 + 1 ) * Rnd + 10 )
			Randomize Timer
			k = Int( ( 800 - 10 + 1 ) * Rnd + 10 )
			Randomize Timer
			l = Int( ( 10 - 2 + 1 ) * Rnd + 2 )
			Call db.connection.execute("INSERT INTO words (x,y,z,word) VALUES ("&j&","&k&","&l&",'" & db.SQLSafe(text) & "')", adExecuteNoRecords)
		next
		endpage("")





		
	case "ajax_loadquizxml"
		Dim quiz_xml : quiz_xml = trim(request("xml"))
		Dim quiz_fn : quiz_fn = mappedfolder & "\SCO1\en-us\Content\" & quiz_xml
		If Not config.filesys.fileexists(quiz_fn) Then ' copy in the default one as this name
			Call Config.FileSys.CopyFile(Server.MapPath("/scorm") & "\Quiz_1.xml", quiz_fn)
		End If
		Set oItem = Server.CreateObject("MSXML2.FreeThreadedDOMDocument")
		oItem.async = False
		oItem.Load(quiz_fn)
		Response.ContentType = "text/xml"
		response.write oItem.xml ' SafeXml(oItem.xml)
		Set oItem = Nothing
		endpage("")
		
	case "ajax_savequizxml"
		Dim sqx_xml, sqx_json, sqx_fn
		sqx_xml = request.form("xml")
		sqx_json = request.form("json")
		sqx_fn = request.form("filename")

		filName = mappedFolder & "\SCO1\en-us\Content\" & sqx_fn
		SaveRevision filName
		IO.SaveFileStream filName, sqx_xml
		IO.SaveFileStream replace(filName,".xml",".json"), sqx_json
		response.write filName
		endpage("")







	case "ajax_loadfix_pagesxml"
		Set oItem = Server.CreateObject("MSXML2.FreeThreadedDOMDocument")
		oItem.async = False
		filName = mappedFolder & "\SCO1\en-us\Pages.xml"
		oItem.Load(filName)
		For each oTemp in oItem.SelectNodes("//page/@fileName") '  & "/text()")
			oTemp.text = IO.RenameSafeFileThenReturnNewName(mappedFolder & "\SCO1\en-us\Content\", oTemp.text, bUpdated)
		Next
		If bUpdated Then
			SaveRevision filName
			IO.SaveFileStream filName, oItem.xml
		End If

		' oItem.Save(mappedFolder & "\SCO1\en-us\Pages.xml") ' sometimes causes zero-byte file
		Response.ContentType = "text/xml"
		response.write SafeXml(oItem.xml)
		Set oItem = Nothing
		endpage("")
		

	case "ajax_method_renamemanyfiles"
		aItems = Array()
		for i = 1 to request.form("filename").count
			tempStr1 = request.form("filename")(i)
			Call Push2Array(aItems, IO.RenameSafeFileThenReturnNewName(mappedFolder & "\SCO1\en-us\Content\", request.form("filename")(i)), bUpdated)
		next
		response.write Join(aItems,",")
		endpage("")

	case "copysettingsfromanothercourse"
		' copy TO me
		Call CopySettingsFromAnotherCourse(Request("source"), CourseId, true)
		EndPage("/engine/pages/edit/?id=" & CourseId)

	case "renamephysicalfile", "ajax_renamephysicalfile"
		tempStr1 = Request("currentFilename")
		tempStr2 = Trim("" & Request("newFilename"))
		sShim = ""
		If (Request("media") = "y") Then
			sShim =  "media\"
		End If
		
		If InStr(tempStr2,".") > 0 Then
			tempStr2 = Left(tempStr2,InStrRev(tempStr2,".") - 1) ' crop any file extension
		End If
		tempStr2 = Replace(strClean("" & tempStr2)," ", "_") ' ensure there's no spaces in the renamed file
		tempStr2 = tempStr2 & Mid(tempStr1, InStrRev(tempStr1,".")) ' append original filename
		If Config.FileSys.FileExists(mappedFolder & "\SCO1\en-us\Content\" & sShim & tempStr1) Then
			If Config.FileSys.FileExists(mappedFolder & "\SCO1\en-us\Content\" & sShim & tempStr2) Then
				Response.Write "That filename (" & tempStr2 & ") already exists."
			else
				Config.FileSys.MoveFile mappedFolder & "\SCO1\en-us\Content\" & sShim & tempStr1, mappedFolder & "\SCO1\en-us\Content\" & sShim & tempStr2
				Response.Write "ok|" & tempStr2
			End If
		else
			Response.Write "The source file doesn't exist (yet?)\n" & mappedFolder & "\SCO1\en-us\Content\" & sShim & tempStr1
		End If
		EndPage("")
		
	case "ajax_deletephysicalfile"
		tempStr1 = Request("currentFilename")
		sShim = ""
		If (Request("media") = "y") Then
			sShim =  "media\"
		End If
		If Config.FileSys.FileExists(mappedFolder & "\SCO1\en-us\Content\" & sShim & tempStr1) Then
			Call Config.FileSys.DeleteFile(mappedFolder & "\SCO1\en-us\Content\" & sShim & tempStr1)
			Response.Write "ok"
		end if
		EndPage("")





	case "loadhelptxt"
		response.write IO.GetFileString(mappedFolder & "\SCO1\Configuration\help.txt", "/engine/templates/help.txt")
		EndPage("")

	case "savehelptxt"
		Call IO.WriteUTF8WithoutBOM(mappedFolder & "\SCO1\Configuration\help.txt", Request.Form("data"))
		response.write "ok"
		EndPage("")

	case "loadreferencejson"
		Response.ContentType = "application/json"
		response.write IO.GetFileString(mappedFolder & "\SCO1\Configuration\references.json", "/engine/templates/references.json")
		EndPage("")
		
	case "savereferencejson"
		Call IO.WriteUTF8WithoutBOM(mappedFolder & "\SCO1\Configuration\references.json", Request.Form("data"))
		response.write "ok"
		EndPage("")

	case "loadglossaryjson"
		Response.ContentType = "application/json"
		response.write IO.GetFileString(mappedFolder & "\SCO1\Configuration\glossary.json", "/engine/templates/glossary.json")
		EndPage("")

	case "saveglossaryjson"
		Call IO.WriteUTF8WithoutBOM(mappedFolder & "\SCO1\Configuration\glossary.json", Request.Form("data"))
		response.write "ok"
		EndPage("")

	case "savemultiplefiles"
		for smfLoop = 1 to Request.Form("filename").Count
			smfFile = request.form("filename")(smfLoop)
			smfDoc = request.form("contents")(smfLoop)
			IO.SaveFileStream mappedFolder & "\SCO1\en-us\Content\" & smfFile, smfDoc
		Next
		response.write "ok"
		EndPage("")









	case "archive"
		if courseid > 0 then
			call db.connection.execute("update courses set container = " & db.sqlsafe(GetContainerId("archive")) & " where id = " & db.sqlsafe(courseid), adExecuteNoRecords)
			' no longer need to touch the folder location
			' Call Config.FileSys.MoveFolder(mappedFolder, Config.ArchivePath & "\")
		else
			Response.Write "<h1>Crap!</h1>"
			Response.Write "<p>the courseid was not specified. This means you're accessing this tool wrong. Go back and do it properly.</p>"
			EndPage("")
		end if 
		
	case "unarchive"
		if courseid > 0 then
			'TODO: pass in the container name
			call db.connection.execute("update courses set container = " & db.sqlsafe(GetContainerId("current")) & " where id = " & db.sqlsafe(courseid), adExecuteNoRecords)
			' no longer need to touch the folder location
			' Call Config.FileSys.MoveFolder(mappedFolder, Config.ArchivePath & "\")
		else
			Response.Write "<h1>Crap!</h1>"
			Response.Write "<p>the courseid was not specified. This means you're accessing this tool wrong. Go back and do it properly.</p>"
			EndPage("")
		end if 









		
	case "history_get"
		text = db.SQLSafe(server.mappath(request("filename")))
		Set HistoryRS = db.getrs("select user,timestamp from history where `key`='{0}' order by timestamp desc", text)
		If Not (HistoryRS.BOF and HistoryRS.EOF) Then
			Response.ContentType = "application/json"
			' this next line is unreadable, but it avoids recordset looping by doing it inside ado, and only works because we only have two fields returning
			response.write Replace("[{""user"":""" & HistoryRS.getString(2,-1, """,""timestamp"":""","""},{""user"":""", vbNull) & "}]", ",{""user"":""}]", "]")
		End If
		Set HistoryRS = Nothing
		endpage("")
		
	case "revision_get"
		response.write db.getScalarWithParams("select data from history where `key`={0} and timestamp={1}", Array(server.mappath(request("filename")), cdate(request("timestamp"))), "")
		EndPage("")
		
	case "revision_delete"
		call db.exec("delete from history where `key`={0} and timestamp={1}", Array(server.mappath(request("filename")), cdate(request("timestamp"))))
		endpage("")	








		
	case "todo_get"
		if request("complete") > "" then
			j = "="
		else
			j = "<>"
		end if
		Set oTodo = JSON.parse("{}")
		aTodo = Array()
		set CourseRS = db.getrs("select distinct course from todo union select 'Core / Engine related' as course", "")
		do while not CourseRS.eof
			text = CourseRS("course")
			set TodoRS = db.getrs("select id,replace(notes,'""','`') from todo where course = '{0}' and (complete " & j & " 1)", text)
			If Not (TodoRS.BOF AND TodoRS.EOF) Then
				Set oCourse = JSON.parse("{}")
				c = GetCourseName(Config.CoursesPath & "\" & text)
				If c = "" Then c = text ' catches that union ...
				oCourse.set "course", c
				text = Replace("[{""id"":" & TodoRS.getString(2,-1, ",""note"":""","""},{""id"":", vbNull) & "}]", ",{""id"":}]", "]") ' speed != readability :(
				oCourse.set "items", JSON.parse(text)
				Call Push2Array(aTodo, JSON.stringify(oCourse))
				Set oCourse = Nothing
			End If
			Set TodoRS = nothing
			CourseRS.movenext
		loop
		oTodo.set "todo", aTodo
		set CourseRS = nothing
		Response.ContentType = "application/json"
		response.write Replace(Replace(Replace(Replace(JSON.stringify(oTodo),"[""{", "[{"), "}""]", "}]"), "\" & chr(34), Chr(34)), "}" & chr(34) & "," & chr(34) & "{", "},{") ' don't ask
		Set oTodo = Nothing
		EndPage("")

	case "todo_add"
		text = Trim("" & request("text"))
		Call db.connection.execute("INSERT INTO todo (course,notes) VALUES ('" & db.SQLSafe(Replace(folder,"/courses/","")) & "','" & db.SQLSafe(text) & "')", adExecuteNoRecords)
		EndPage("") ' action.asp?folder=" & folder & "&action=todo_get")

	case "todo_remove"
		'response.write i
		Call db.connection.execute("DELETE FROM todo WHERE id = " & Clng(request("id")), adExecuteNoRecords)
		'Call db.delete("todo",i)
		EndPage("")

	case "todo_change"
		text = Trim("" & request("text"))
		Call db.connection.execute("UPDATE todo SET notes='" & db.SQLSafe(text) & "' WHERE id = " & Clng(request("id")), adExecuteNoRecords)
		EndPage("")

	case "todo_toggle"
		Call db.toggle("todo","complete",Clng(request("id")))
		EndPage("")

	case "todocount"
		response.write db.count("todo", "(complete <> 1)")
		EndPage("")		









	case "flag"
		c = trim("" & request("colour"))
		If c = "" Then
			Call db.connection.execute("DELETE FROM label WHERE key = '" & db.SQLSafe(folder) & "'", adExecuteNoRecords)
			response.write "false"
		else
			Call db.connection.execute("REPLACE INTO label (key, colour) VALUES ('" & db.SQLSafe(folder) & "','" & db.SQLSafe(c) & "')", adExecuteNoRecords)
			response.write "true"
		end if
		EndPage("")









	case "changescormruntime"
		scormVersion = request.form("version")
		ApplySCORM folder, scormVersion
   		response.write "ok"
   		EndPage("")









    case "ajaxloadcoursesettings"
		ConvertSettingsToJSONIfMissing mappedFolder
    	MakeSureSettingsJSONIsUpToDate mappedFolder
    	Response.ContentType = "application/json"
   		Response.Write IO.GetFileString(mappedFolder & "\SCO1\Configuration\Settings.json", "")
   		EndPage("")
   		
	case "ajaxsavecoursesettings", "ajax_savecoursesettings" ' new way of saving settings; proxied through JSON so that it reformats the file nicely!
		sJSON = Request.Form("settings")
		Set oJSON = JSON.parse(join(array(sJSON)))
		colour = oJSON.layout.basecolour
		IO.WriteUTF8WithoutBOM mappedFolder & "\SCO1\Configuration\settings.json", JSON.stringify(oJSON, null, 4)
		If colour > "" then
			SynchroniseBaseColourFromSettings mappedFolder
		End If
		
		Call PushCurrentConfigToDatabase(folder)
		
		Set oJSON = Nothing
		Response.Write "ok"
		EndPage("")

	case "ajaxsavecoursesettingsxml" ' old way of saving settings

		set fil = Config.FileSys.OpenTextFile(mappedFolder & "\Course.xml", 1, false)
		courseXml = fil.ReadAll
		If Left(courseXml,3) = UTF8_BOM Then courseXml = Mid(courseXml,4)
		fil.close

		set fil = Config.FileSys.OpenTextFile(mappedFolder & "\SCO1\Configuration\Settings.xml", 1, false)
		settingsXml = fil.ReadAll
		If Left(settingsXml,3) = UTF8_BOM Then settingsXml = Mid(settingsXml,4)
		fil.close

		' FormDataDump true, false
		
		for i = 0 to clng(request.form("count"))
			filename = clng(request.form("field_" & i & "_index"))
			xpath = URLDecode(request.form("field_" & i & "_xpath"))
			value = Trim(request.form("field_" & i) & "")
			valuetype = request.form("field_" & i & "_type")
			If (valuetype = "checkbox" or valuetype = "radio") and value = "" then value = "false"
			
			If filename = 0 then
				Call WorkOutAndUpdateXML(courseXml, xpath, value)
			ElseIf filename = 1 Then
				Call WorkOutAndUpdateXML(settingsXml, xpath, value)
			End If
			
		next
		
		IO.WriteUTF8WithoutBOM mappedFolder & "\Course.xml", courseXml
		IO.WriteUTF8WithoutBOM mappedFolder & "\SCO1\Configuration\Settings.xml", settingsXml
		response.write "<scr"&"ipt>parent.alert('That thing what you wanted done is done, innit?')</scr"&"ipt>"
		EndPage("")


	case "ajax_convertblock"
		filName = request.form("filename")
		text = request.form("content")
		filename = IO.FindASafeFileNameThatIsntAlreadyUsed(server.mappath(filName), "parse")
		IO.SaveFileStream filename, text
		response.write Mid(filename, InStrRev(filename, "\") + 1)
		endpage("")

	case "ajaxsavefile"
		filData = Request.Form("content")
		filName = Request.Form("filename")
		If InStr(filName,":\") = 0 Then filName = Server.MapPath(filName)
		CheckSaveRevision filName, filData
		IO.SaveFileStream filName, filData
		response.write " "
		EndPage("")
		
	case "contentexists"
		filName = mappedFolder & "\SCO1\en-us\Content\" & Request.querystring("filename")
		If Config.FileSys.FileExists(filName) Then
			response.write "true"
		else
			response.write "false"
		end if
		EndPage("")

	case "ajaxsavepagesxml"
		xmlData = request.form("xml")

		xmlData = Mid(xmlData, InStr(6, xmlData, "<")) ' skip first <page>
		xmlData = Left(xmlData, Len(xmlData) - 14) ' strip last two </page>s; because of replacing </ul> with </page>
		xmlData = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbNewLine & "<sco>" & vbNewLine & xmlData & vbNewLine & "</sco>"

		' ensure correct attribute casing
		xmlData = Replace(xmlData, "&", "&amp;")
		xmlData = Replace(xmlData, " filename=", " fileName=")
		xmlData = Replace(xmlData, " contributescore=", " contributeScore=")
		xmlData = Replace(xmlData, " contributepercentage=", " contributePercentage=")

		xmlData = Replace(xmlData, "/><page ", "/>" & vbNewLine & "<page ")
		xmlData = Replace(xmlData, "><page ", ">" & vbNewLine & "<page ")
		xmlData = Replace(xmlData, "></page>", ">" & vbNewLine & "</page>")

		filName = mappedFolder & "\SCO1\en-us\Pages.xml"
		SaveRevision filName
		IO.SaveFileStream filName, xmlData
		EndPage("")








	
	case "unlock"
		Call db.connection.execute("UPDATE courses SET locked = 0 WHERE id = " & db.sqlsafe(courseid), adExecuteNoRecords)
		
	case "lock"
		Call db.connection.execute("UPDATE courses SET locked = 1 WHERE id = " & db.sqlsafe(courseid), adExecuteNoRecords)








	
	case "delete" ' can only be deleted from archive; move course to archive, then delete
		Call db.connection.execute("DELETE FROM courses WHERE id = " & db.sqlsafe(courseid), adExecuteNoRecords)	
		set fold = Config.FileSys.getfolder(mappedfolder)
		fold.delete
		
	case "download"
		tEngine = GetEngineName(mappedFolder) ' e.g. 'textplayer'
		If tEngine = "?" Then tEngine = "leo" ' fallback
		
		' Call FixManifest(mappedFolder, folder, tEngine)
		reApplySCORM mappedfolder
		
		'Dim zipPath : zipPath = QuoteOptional(Config.TempPath & "\" & replace(folder,".","_") & ".zip")
		zipPath = replace(mappedfolder,".","_") & ".zip"
		If Config.FileSys.FileExists(zipPath) Then Config.FileSys.DeleteFile(zipPath)

		sourcePath = mappedFolder & "\*.*"
		arrActions = Array()
		If tEngine = "textplayer" Then Call ChangeFilesForZipAndDownload(mappedFolder, arrActions) ' do, arrActions is empty
		zResult = IO.XZip( sourcePath, zipPath )
		If tEngine = "textplayer" Then Call ChangeFilesForZipAndDownload(mappedFolder, arrActions) ' undo, arrActions is not empty

		If zResult = 0 Then
			response.contenttype = "application/octet-stream"
			response.addheader "Content-Disposition", "attachment; filename=" & replace(folder,".","_") & ".zip"
			response.redirect replace(folder,".","_") & ".zip" ' folder & "/" & Replace(Mid(folder, InStrRev(folder, "/") + 1),".","_") & ".zip"
		Else
			Response.Write "Unable to zip " & sourcePath & ", IO.XZip returned " & zResult
		End If
		EndPage("")

	case "play"
		Dim play_special_sco
		play_special_sco = false
		set fil = Config.FileSys.OpenTextFile(mappedFolder & "\imsmanifest.xml", 1, false)
		data = fil.ReadAll
		fil.close

		res = mid(data, instr(data, "<resource "), instr(instr(data, "<resource "), data, ">", vbBinaryCompare) - instr(data, "<resource "))
		href = replace(mid(res, instr(res, "href=")+5), chr(34),"")
		If Not Config.FileSys.FileExists(mappedFolder & "\" & href) Then
			href = "SCO1/LaUnCh.html" ' fall back on standard player name, written so we know its had a problem
			
			' hmm, there are different sorts of ims manifest
			if config.filesys.folderexists(mappedFolder & "\pages") Then
				if config.filesys.fileexists(mappedFolder & "\pages\welcome.html") Then
					href = "pages/WeLcOmE.hTmL" ' again, odd formatting to show we set this rather than examined it
					play_special_sco = true
				end if
			end if
			
		End If

		' Make sure image manifest is up to date
		UpdateImageManifest mappedFolder

		' open the launch page with or without the debug harness (hold shift while clicking to enable debug)
		if wrap then
			if not config.filesys.folderexists(mappedFolder & "\SCO1\Configuration\") Then
				wrapScoVersion = "1.2"
			Else
				wrapScoVersion = GetEngineSCO(mappedFolder)
			End If
			If wrapScoVersion = "1.2" or play_special_sco Then
				redirectTo = "../scorm/scorm12testwrap.asp?sco=" & Server.UrlEncode(folder & "/" & href)
			else
				redirectTo = "../scorm/scorm2004testwrap.htm?sco=" & Server.UrlEncode(folder & "/" & href)
			End If
		else
			redirectTo = folder & "/" & href
		end if

	case "edit"
		redirectTo = "/engine/pages/edit/?id=" & courseid





	case "index_post_changetheme"
		If request("fullreset") > "" Then
			Call fnSettings_ApplyBaseline(folder)
		End If
		layoutsTemplate = request("layout")
		Call fnSettings_ApplyLayout(layoutsTemplate, folder, true)

	case "applylayout"
		layoutsTemplate = request("layout")
		Call fnSettings_ApplyLayout(layoutsTemplate, folder, true)
		response.contenttype = "application/json"
		response.write ReturnSettingsJsonString(mappedfolder)
		EndPage("")

	case "getlayoutabouttxt"
		layoutsTemplate = Request("template")
		response.write IO.GetFileString(Config.LayoutsPath & "\" & layoutsTemplate & "\about.txt", "")
		EndPage("")

		
'	case "changeruntime"
'		contentPath = mappedFolder
'		selectedRuntime = request.querystring("runtime2")
'		copyLess = (request.querystring("overwriteLess") = "true")
'		copyConfig = (request.querystring("overwriteConfig") = "true")
'		copyLayout = (request.querystring("layout") > "")
'		' response.write "<li>copy less " & copyless
'   		ApplyRuntime contentPath, folder, selectedRuntime, false, copyLess
'   		
'   		If selectedRuntime = "textplayer" Then
'   		
'   			If copyConfig Then
'   				
'   				Call Config.FileSys.DeleteFile(contentPath & "\SCO1\Configuration\settings.json")
'   				
'   				Sleep 1 ' wait for file lock to release
'
'				ConvertSettingsToJSONIfMissing contentPath ' create from xml
'				MakeSureSettingsJSONIsUpToDate contentPath ' upgrade from latest
'
'   			End If
'   		
'   			If copyLayout Then
'   				Call ApplyLayout(request.querystring("layout"), folder, false)
'   			End If
'   		
'   			If copyLess Then ' synch colours back from config
'				SynchroniseBaseColourFromSettings mappedFolder
'			End If
'
'		End If

   	case "clone"
   		cloneName = SafeName(Request("newname")) 
   		If cloneName > " " Then
   			path = Config.CourseRoot & "\" & GetContainerNameFromCourseId(courseid)
	   		Config.FileSys.CopyFolder mappedFolder, path & "\" & cloneName
	   		Call db.connection.execute("INSERT INTO courses (name,folder,touched,engine,layout,stage,container,config,locked) SELECT '" & db.sqlsafe(Request("newname")) & "' as name,'" & db.sqlsafe(cloneName) & "' as folder,touched,engine,layout,stage,container,config,locked FROM courses WHERE id=" & db.sqlsafe(courseid), adExecuteNoRecords)
	   		response.write db.LastInsertId("courses") ' db.getScalar("SELECT last_insert_rowid() FROM courses", 0)
	   	End If
	   	EndPage("")




	   	
	case "ajax_newcourse"
   		newName = SafeName(Request.Form("name"))
   		If newName > " " Then
	   		Config.FileSys.CopyFolder Server.MapPath("/engine/templates/newcourse"), Config.CoursesPath & "\" & newName
			Call GetSettingsJSON(Config.CoursesPath & "\" & newName, ncSettings)
			ncSettings.course.name = Trim(Request.Form("name"))
			Call SaveSettingsJson(Config.CoursesPath & "\" & newName, ncSettings)
			Set ncSettings = Nothing
   		End If
   		response.write newName
   		EndPage("")

	case "post_newcourse"
   		newName = SafeName(Request.Form("course"))
   		containerName = Request.Form("container")
   		newName = EnsureCourseNameUniqueness(newName)
   		containerPath = Server.MapPath("/courses/" & containerName)
   		If newName > " " Then
	   		Config.FileSys.CopyFolder Server.MapPath("/engine/templates/newcourse"), containerPath & "\" & newName
	   		' sleep 1
			Call GetSettingsJSON(containerPath & "\" & newName, ncSettings)
			ncSettings.course.name = Trim(Request.Form("course")) ' not safe
			Call SaveSettingsJson(containerPath & "\" & newName, ncSettings)
			ncId = CreateANewCourseRecord(newName, containerName, ncSettings)
			Set ncSettings = Nothing
			EndPage("/engine/pages/edit/?id=" & ncId)
   		End If
   		
   	case "ajax_movecontainer"
   		containerName = Request.Form("container")
   		containerPath = Server.MapPath("/courses/" & containerName) ' after move
   		path = GetCoursePath(courseid) ' before move
   		Config.FileSys.MoveFolder server.mappath(path), containerPath & "\"
   		Call db.exec("UPDATE courses SET container={0} where id={1}", Array(GetContainerId(containerName),courseid))
   		Response.Write "The course has been moved to " & containerName
   		EndPage("")








   		
   	case "renamefolder"
   		newName = SafeName(Request.Form("rename"))
   		If newName > " " Then
	   		Config.FileSys.MoveFolder mappedFolder, Config.CoursesPath & "\" & newName
	   	End If
   		EndPage("/engine/pages/edit/?id=" & courseid)




	case "getlesslivestatus"
		Response.Write GetCourseProperty(folder, "lessdebug")
		EndPage("")

	case "togglelesslivestatus"
		path = mappedFolder
		contentStr = IO.GetFileString(path & "\SCO1\en-us\" & GetSettingValue(path, "content"), "")
		If InStr(LCase(contentStr), "less.watch();") > 0 Then
			contentStr = Replace(contentStr, lessDevScript, lessScript)
			IO.SaveFileStream path & "\SCO1\en-us\" & GetSettingValue(path, "content"), contentStr
			response.write "false"
		ElseIf InStr(contentStr, lessScript) > 0 Then
			contentStr = Replace(contentStr, lessScript, lessDevScript)
			IO.SaveFileStream path & "\SCO1\en-us\" & GetSettingValue(path, "content"), contentStr
			response.write "true"
		else
			response.write "unsupported"
		End If
		EndPage("")



		
	case "getscormdebug"
		Response.Write GetCourseProperty(folder, "scodebug")
		EndPage("")
		
	case "setscormdebug"
		path = mappedFolder
		path = path & "\SCO1\" & GetSettingValue(path, "runtime") & "\js\scorm\sco_api.js"
		contentStr = IO.GetFileString(path, "")
		If InStr(Lcase(contentStr), "var _bDebug = false;") > 0 Then
			contentStr = Replace(contentStr, "var _bDebug = false;", "var _bDebug = true;")
			response.write "true"
		else
			contentStr = Replace(contentStr, "var _bDebug = true;", "var _bDebug = false;")
			response.write "false"
		end if
		IO.WriteUTF8WithoutBOM path, contentStr
		EndPage("")
		
	case "compileless"
		CompileLessUsingNode_LessC mappedFolder, strOutput
		Response.Write strOutput
		EndPage("")
   	
end select

set config = nothing
response.redirect (redirectTo)

Function WorkOutAndUpdateXML(ByRef fileString, byVal xPath, byVal valueToSet)
Dim attrib, attribNew, attribOld
Dim tag, nodeOrig, nodeValue, nodeNew, ar
Dim isCdata

	If InStr(xPath, "/@") > 0 Then
		ar = Split(xPath, "/@")
		attrib = ar(1)
		attribNew = " " & attrib & "=" & chr(34) & valueToSet & chr(34)
		xPath = Replace(xPath, "/@" & attrib, "")
	End If
	xPath = Mid(xPath, InStr(3, xPath, "/")) ' crop first param, we don't care and it might clash
	Tag = Mid(xPath, InStrRev(xPath, "/") + 1)
	If InStr(Tag, "[@") > 0 Then
		Tag = Mid(Tag, 1, InStrRev(Tag, "[@") - 1)
	End If
	If attrib > "" Then
		nodeOrig = Mid(fileString, InStr(fileString, "<" & Tag & " ") - 1)
		nodeOrig = Mid(nodeOrig, 1, InStr(nodeOrig, ">"))
		attribOld = Mid(nodeOrig, InStr(nodeOrig, attrib & "="))
		attribOld = " " & Mid(attribOld, 1, InStr(Len(attrib) + 3, attribOld, chr(34)))
		
		' Build a repacement node
		nodeNew = Replace(nodeOrig, attribOld, attribNew)
		
	Else
		nodeOrig = Mid(fileString, InStr(fileString, "<" & Tag))
		nodeOrig = Mid(nodeOrig, 1, InStr(nodeOrig, "</" & Tag & ">") + 3 + Len(Tag))
		
		' get the inner portion of the node (the text to replace)
		nodeValue = Replace(Replace(nodeValue, "<![CDATA[", ""), "]]>","")
		nodeValue = Mid(nodeOrig, InStr(InStr(nodeOrig, "<" & tag), nodeOrig, ">") + 1)
		nodeValue = Trim(Mid(nodeValue, 1, InStrRev(nodeValue, "</" & tag)-1))


		isCData = (InStr(valueToSet, "<") > 0) or (InStr(valueToSet, "&") > 0)
		If isCData Then
			nodeNew = Replace(nodeOrig, nodeValue, "<![CDATA[" & valueToSet & "]]>")
		Else
			nodeNew = Replace(nodeOrig, nodeValue, valueToSet)
		End If
		
	End If

	' Update the original filestring (byref) to store changed value
	fileString = Replace(fileString, nodeOrig, nodeNew)

	if false then
		response.write "Tag=" & tag & "|" & vbnewline
		response.write "Attrib=" & attrib & "|" & vbnewline
		response.write "attribNew=" & attribNew & "|" & vbnewline
		response.write "attribOld=" & attribOld & "|" & vbNewLine
		response.write "nodeOrig=" & server.htmlencode(nodeOrig) & "|" & vbnewline
		response.write "nodeValue=" & server.htmlencode(nodeValue) & "|" & vbnewline
		response.write "nodeNew=" & server.htmlencode(nodeNew) & "|" & vbnewline
		response.write "-----------------------------------------------------------" & vbnewline
	End If
	
End Function


Function URLDecode(sConvert)
    Dim aSplit
    Dim sOutput
    Dim I
    If IsNull(sConvert) Then
       URLDecode = ""
       Exit Function
    End If

    ' convert all pluses to spaces
    sOutput = REPLACE(sConvert, "+", " ")

    ' next convert %hexdigits to the character
    aSplit = Split(sOutput, "%")

    If IsArray(aSplit) Then
      sOutput = aSplit(0)
      For I = 0 to UBound(aSplit) - 1
        sOutput = sOutput & _
          Chr("&H" & Left(aSplit(i + 1), 2)) &_
          Right(aSplit(i + 1), Len(aSplit(i + 1)) - 2)
      Next
    End If

    URLDecode = sOutput
End Function


Sub FormDataDump(bolShowOutput, bolEndPageExecution)
  Dim sItem

  'What linebreak character do we need to use?
  Dim strLineBreak
  If bolShowOutput then
    'We are showing the output, so set the line break character
    'to the HTML line breaking character
    strLineBreak = "<br>"
  Else
    'We are nesting the data dump in an HTML comment block, so
    'use the carraige return instead of <br>
    'Also start the HTML comment block
    strLineBreak = vbCrLf
    Response.Write("<!--" & strLineBreak)
  End If
  

  'Display the Request.Form collection
  Response.Write("DISPLAYING REQUEST.FORM COLLECTION" & strLineBreak)
  For Each sItem In Request.Form
    Response.Write(sItem)
    Response.Write(" - [" & server.htmlencode(Request.Form(sItem)) & "]" & strLineBreak)
  Next
  
  
  'Display the Request.QueryString collection
  Response.Write(strLineBreak & strLineBreak)
  Response.Write("DISPLAYING REQUEST.QUERYSTRING COLLECTION" & strLineBreak)
  For Each sItem In Request.QueryString
    Response.Write(sItem)
    Response.Write(" - [" & server.htmlencode(Request.QueryString(sItem)) & "]" & strLineBreak)
  Next

  
  'If we are wanting to hide the output, display the closing
  'HTML comment tag
  If Not bolShowOutput then Response.Write(strLineBreak & "-->")

  'End page execution if needed
  If bolEndPageExecution then EndPage("")
End Sub

Sub EndPage(ByVal redirectTo)
	If not db is nothing then
		db.close()
		set db = nothing
	end if
	if not str is nothing then set str = nothing
	if not lib is nothing then
		set lib.logger = nothing
		set lib = nothing
	end if
	If redirectTo > "" Then
		response.redirect (redirectTo)
	else
		Response.End
	end if
End Sub

''
''	Used by paste-upload, this decodes base64 it binary and saves a new file
'' returns: virtual path to file
'' fun fact: msxml2 has a base64 decoder in it!
''
Function SaveBase64ToFile(byval data, byval extn, byval vpath)
Dim objXML, objDocElem, objStream, strNameV, strNameA
	Set objXML = CreateObject("MSXml2.DOMDocument")
	Set objDocElem = objXML.createElement("Base64Data")
	objDocElem.DataType = "bin.base64"
	objDocElem.text = data
	Set objStream = CreateObject("ADODB.Stream")
	objStream.Type = adTypeBinary
	objStream.Open()
	strNameV = vpath & "/" & NewRefId(true) & "." & extn
	strNameA = Server.MapPath(strNameV)
	objStream.Write objDocElem.NodeTypedValue
	objStream.SaveToFile strNameA, adSaveCreateOverWrite
	SaveBase64ToFile = strNameV
	Set objXML = Nothing
	Set objDocElem = Nothing
	Set objStream = Nothing
End Function		

%>

<!-- #include file="lib/page_terminate.asp" -->
