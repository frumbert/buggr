<div id='tab-toolbar' class='row-fluid well well-small'>
	<div class='span6'>
		<select data-id='layout.template' id='template-switcher'>
		{{#each layouts}}<option>{{.}}</option>{{/each}}
		</select>
		<button class="btn" type="button" id='template-apply'>Apply layout</button>
	</div>
	<div class='span3 offset1'>
		<div class="btn-group">
			<button class="btn dropdown-toggle" data-toggle="dropdown">
				Other actions
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li><a href='#' id='less-edit' disabled>Edit LESS</a></li>
				<li><a href='#' id='less-compile'>Compile LESS</a></li>
				<li class='divider'></li>
				<li><a href='#' id='less-toggle'>Debugging is {{#if lessDebug}}ON{{else}}OFF{{/if}}</a></li>
			</ul>
		</div>
	</div>
	<div class='span2'>
		<a class='btn btn-primary' href='#' id='tab-save'>Save</a>
	</div>
</div>

<div id='tab-body'>

</div>