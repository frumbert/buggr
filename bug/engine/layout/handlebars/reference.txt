<p>Blank out the fields on an item to remove it; add new items at the bottom</p>
<ul class="reference-editor">
	{{#each references}}
	<li uniqueid="{{uniqueid}}">
		<input type="text" name="title" class="input-block-level focus-reveals" placeholder="Enter a name" value="{{title}}">
		<div class="focus-block">
		<input type="text" name="hyperlink" class="input-block-level" placeholder="Enter a link" value="{{hyperlink}}">
		<textarea name="description" class="input-block-level" rows="2" placeholder="Description is optional">{{description}}</textarea>
		</div>
	</li>
	{{/each}}
	<li class="new-item selected" uniqueid="">
		<input type="text" name="title" class="input-block-level" placeholder="Enter a name">
		<input type="text" name="hyperlink" class="input-block-level" placeholder="Enter a link">
		<textarea name="description" class="input-block-level" rows="2" placeholder="Description is optional"></textarea>
	</li>
</ul>