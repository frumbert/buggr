$(function() {

	$(document).bind("update-recent-actions", function () {
		$("#recent-actions")
			.html("<i class='icon-spinner icon-spin icon-large'></i>")
			.load("ajax.asp", {
				action: "bugs_get_recentactions"
			}, function () {
				setTimeout(function () {
					$(document).trigger("update-recent-actions")
				}, 60000);
			});
	}).trigger("update-recent-actions");

});