#About Buggr!

This is a miniature issue tracker, designed with Chrome as the intended browser (Firefox and Vivaldi *may* also work; IE/Edge I don't care about so *probably* not), and built on SqLite and classic asp. Useful for small teams who don't need complex issue tracking software.

## Code

To run this you need a system capable of running Classic ASP, JScript and VBScript. It heavily uses Handlebars.js, JSON2.js, and features a light non-mvc, template-based architecture for simplicity. It's sqlite so don't expect it to scale to enterprise level. Parts of the code have been lifted from AXE.

## Setup

You need a sqlite odbc driver for windows deployment (SQLite3 ODBC Driver).
Modify lib/classConfig.asp to change the notification email addresses and such to your liking.
Make the /attachments virtual folder writable by the IIS user.

## License

A couple of files are from https://github.com/nagaozen/asp-xtreme-evolution - which is GPL.
My code is all Public Domain.