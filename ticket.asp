<!-- #include file="page_start.asp" -->

<%
Dim ticketId : ticketId = Request.QueryString("id")
Dim strAction : strAction = Request("action")
Dim strStatus : strStatus = Request("status")
Dim jobOpenerName
Select Case strAction
	Case "setstatus"
		Call Buggr.SetTicketStatus(strStatus, ticketId, Config.MyUserName)
		
	Case "reply"
		Call Buggr.ReplyToTicket(ticketId, Request.Form("details"), Request.Form("screenshot-url"), (Request.Form("done")="y"), (Request.Form("notify")="y"), Config.MyUserName)
		response.clear
		response.redirect "ticket.asp?id=" & ticketId

	case "buggrit"
		Call Buggr.MakeTicketReplyItsOwnBug(Request.Form("reply-id"), Config.MyUserName)
		
	case "create"
		ticketId = Buggr.SubmitBug( Request.Form("details"), Request.Form("level"), Request.Form("screenshot-url"), Config.MyUserName)
		response.clear
		response.redirect "ticket.asp?id=" & ticketId

End Select

%>    
	<div class="jumbotron">
		<div class="container">
			<div class="row">
<%

set tempRS = db.getrs("select * from ticket where id={0}", Array(ticketId))
response.write "<form class='well form-inline pull-right' method='post'>"
response.write "<input type='hidden' name='action' value='setstatus'>"
response.write "<label for='inputStatus' class='control-label'>Current&nbsp;status</label> "
response.write "<select id='inputStatus' name='status'>"
Dim iLoop, arStatus : arStatus = Array("new","open","done","closed")
For iLoop = 0 To UBound(arStatus)
	Response.Write "<option"
	If arStatus(iLoop) = tempRS("status") Then Response.Write " selected"
	Response.Write ">" & arStatus(iLoop) & "</option>"
Next
Response.Write "</select> <button type='submit' class='btn btn-default btn-xs'>Update</button>"
response.write "</form>"

response.write "<h1><i class='icon-" & Buggr.IconForLevel(tempRS("level")) & "'></i> " & tempRS("level") & "-" & Right("000" & tempRS("id"),3) & "</h1>"
Set lineRS = db.getrs("select id, datetime(added,'localtime') as dte, details, url, who from ticket_item where ticket_id={0} order by added limit 1", Array(ticketId))
lastRead = ""
if not lineRS.eof then
	response.write "<p>Logged: " & lineRS("dte") & "</p>"
	If lineRS("who") > "" Then
		response.write "<p><small class='label label-default'>" & lineRS("who") & "</small></p>"
		jobOpenerName = lineRS("who")
	end if

	response.write Buggr.formatLines(lineRS("details"), lineRS("url"))
end if
set lineRS = nothing
Set tempRS = Nothing
%>
			</div>
		</div>
	</div>
			
	<div class="container">
		<%
		Set lineRS = db.getrs("select id, datetime(added,'localtime') as dte, details, url, who from ticket_item where ticket_id={0} order by added limit -1 offset 1", Array(ticketId))
		lastRead = ""
		If (lineRS.eof and lineRS.bof) Then
			response.write "<div class='row'><h2>Uh-oh</h2><p>Nobody has commented on this bug yet!</p></div><hr>"
		Else
		do while not lineRS.eof
			response.write "<div class='row'><div class='col-md-3'>"
			lastRead = lineRS("dte")
			response.write "<p>" & lastRead & "</p>"
			If lineRS("who") > "" Then response.write "<p><small class='label label-default'>" & lineRS("who") & "</small></p>"
			If Config.MyUserName = "tim" Or Config.MyUserName = jobOpenerName Then ' yay, hacks!
				response.write "<form method='post'><input type='hidden' name='action' value='buggrit'><input type='hidden' name='reply-id' value='" & lineRS("id") & "'>"
				response.write "<input type='submit' class='btn btn-default btn-xs' value='Buggr this!' title='Turns this reply into its own seperate bug'>"
				response.write "</form>"
			End If
			response.write "</div><div class='col-md-9'>"
			response.write Buggr.formatLines(lineRS("details"), lineRS("url"))
			response.write "</div></div>"
			response.write "<hr>"
			lineRS.movenext
		Loop
		End If
		
		if isdate(lastRead) Then
			Call Buggr.UpdateLastReadForTicket(Config.MyUserName, CLng(ticketId), cdate(lastRead))
		End if
		
		Set lineRS = Nothing
		%>
		<div class='row'><form id="conversation form-horizontal" method="post" class='well clearfix'>
			<div class='col-md-3'>
				<p><small class='label label-default'><%=Config.MyUserName%></small></p>
			</div>
			<div class='col-md-6'>
				<div class="form-group">
					<label for="ta-details" class="sr-only">Details</label>
					<textarea name="details" rows="4" class="form-control" placeholder="Enter any additions you have to this conversation in this box. If a screenshot is required, take it as normal, click the dashed box on the right and press ctrl-v."></textarea>
				</div>
				<div class='form-inline'>
					<div class='col-md-3'>
						<button type="submit" class="btn btn-primary">Add entry</button>
					</div>
					<div class='col-md-9'>
						<label class="checkbox pull-left"> <i class='icon-thumbs-up'></i> <input type="checkbox" name="done" value="y">  Mark as Done?</label>
						<label class="checkbox pull-right"> <i class='icon-envelope'></i> <input type="checkbox" name="notify" value="y">  Notify?</label>
					</div>
				</div>
			</div><div class='col-md-3'><div id='reply_screenshot'>
				<span>Copy screenshot<br>then click here</span>
			</div>
			<input type="hidden" name="action" value="reply">
		</form></div>
	</div>

	<form enctype='multipart/form-data' method='post' name='fileinfo' id='fileinfo'></form>

<!-- #include file="page_end.asp" -->