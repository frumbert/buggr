	<div id="addr-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<form class="modal-content" method="post" action="bugs.asp">
				<input type="hidden" name="method" value="create">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add a new bug</h4>
				</div>
				<div class="modal-body container">
					<div class="row">
						<div class='col-md-6'>
							<div class="form-group">
								<label for="ta-details" class="sr-only">Details</label>
								<textarea id="ta-details" name="details" rows="4" class="form-control" placeholder="Enter the bug description in this box. If a screenshot is required, copy it (e.g. alt-printscreen, etc), click the dashed box on the right and press ctrl-v. Use the radio buttons below to set the job priority."></textarea>
							</div>
						</div>
						<div class='col-md-3'>
							<div id='paste_screenshot'>
								<span>Copy screenshot<br><small>(if required)</small><br>then click here</span>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-inline pull-left">
						<label class="radio-inline" title="You're frowny-faced because this is a big problem">
						  <input type="radio" value="major" name="level"><i class='icon-frown'></i> Major
						</label>
						<label class="radio-inline" title="This bug happens, but you're ok with it for now">
						  <input type="radio" value="meh" name="level" checked><i class='icon-meh'></i> Meh
						</label>
						<label class="radio-inline" title="You're wondering if you should even mention this">
						  <input type="radio" value="minor" name="level"><i class='icon-smile'></i> Minor
						</label>
					</div>
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
					<input type="submit" class="btn btn-primary" value="Buggr it!">
				</div>
			</form>
		</div>
	</div>
      
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-sortable.js"></script>
	<script src="js/buggr_paste.js" type="text/javascript"></script>
	<script src="js/<%=CURRENT_PAGE%>.js"></script>

  </body>
</html>

<!-- #include file="lib/page_terminate.asp" -->
