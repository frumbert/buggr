<!-- #include file="lib/page_initialise.asp" -->
<%
' response.contentType = "application/json"

Dim Action, Id

Action = Request("action")
Id = request("id")

Select Case LCase(Action)
	Case "bugs_get_recentactions"
		Dim tempRS, ar(9), obj, i
		Set tempRS = db.getRS("select id, datetime(added) as dte, details, who, ticket_id from ticket_item order by added desc limit 10",0)
		Do While Not tempRS.eof
			set obj = JSON.parse("{}")
			obj.set "date", NicerDate(tempRS("dte"))
			obj.set "title", Right("000" & tempRS("ticket_id"), 3)
			obj.set "id", clng(tempRS("ticket_id"))
			obj.set "who", Trim("" & tempRS("who"))
			obj.set "summary", str.shorten(str.stripTags(tempRS("details")), 99, "...")
			set ar(i) = obj
			i = i + 1
			tempRS.moveNext
		Loop
		Set tempRS = Nothing
		Set obj = JSON.parse("{}")
		obj.set "actions", ar
		Call lib.Render("recent_actions", obj)
		Set obj = Nothing
	
End Select


%>
<!-- #include file="lib/page_terminate.asp" -->