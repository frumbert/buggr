<!-- #include file="page_start.asp" -->
<%
Dim i, filts, lineRS, q
Dim rqFilter : rqFilter = request.querystring("filter")
q = trim(request.querystring("q"))

%>    

<div class="container">
	<div class="row">
		<h1>Hello <%=Config.MyUserName%>. <small>Welcome to Buggr, the micro-helpdesk.</small></h1>
		<p>Press on a ticket-number to open it, or log jobs with the button above.</p>
	</div>
	<div class="row">
		<div class="col-md-9">
		<%
		filts = Array("new","open")
		sql = " status='{0}' or status='{1}'"
		If request.querystring("filter").count > 0 Then ' it's a COLLECTION, not an ARRAY, gah
			filts = Array()
			sql = ""
			For i = 1 to request.querystring("filter").count
				Push2Array filts, request.querystring("filter")(i)
				sql = sql & " status='{" & (i-1) & "}' OR"
			Next
			if right(sql,3) = " OR" Then
				sql = Left(sql, len(sql)-3)
			end if
		end if
		if q > "" Then
			filts = array(q)
			sql = "id in (select ticket_id from ticket_item where details like '%{0}%')"
		end if
		set tempRS = db.getrs("select * from ticket where (" & sql & ") order by case when status='new' then 0 when status='open' then 1 when status='done' then 2 when status='closed' then 3 end", filts)
		aItems = Array()
		If Not (tempRS.BOF AND tempRS.EOF) Then
			response.write "<table class='table table-bordered table-striped table-hover sortable'>"
			response.write "<thead><tr><th>Ticket</th><th>Who</th><th>Details</th><th>Severity</th><th>Status</th></tr></thead>"
			response.write "<tbody>"
			Do While Not tempRS.EOF
				response.write "<tr>"
				response.write "<td><a href='ticket.asp?id=" & tempRS("id") & "' class='btn btn-default'>" & Right("000" & tempRS("id"),3) & "</a></td>"
				response.write "<td>" & tempRS("who") & "</td>"

				response.write "<td>"
				Set lineRS = db.getrs("select details from ticket_item where ticket_id={0} order by added limit 1", array(tempRS("id")))
				if not (lineRS.eof and lineRS.bof) Then
					response.write lineRS("details")
				End If
				Set lineRS = Nothing
				If Buggr.TicketHasUnreadItems(tempRS("id"), Config.MyUserName) Then
					response.write "<br><span class='label label-info'>Unread replies</span>"
				End If
				response.write "</td>"

				select case tempRS("level")
					case "minor"
						response.write "<td class='text-info' data-value='-1'>"
					case "meh"
						response.write "<td class='text-warning' data-value='0'>"
					case "major"
						response.write "<td class='text-error' data-value='1'>"
				End select
				response.write "<i class='icon-" & Buggr.IconForLevel(tempRS("level")) & "'></i></td>"
				
				response.write "<td>" & tempRS("status") & "</td>"

				response.write "</tr>"
				tempRS.moveNext
			Loop
			response.write "</tbody>"
			response.write "</table>"
		elseif q > "" Then
			response.write "<h2>Search for " & q & "...? I got nothing.</h2>"
		Else
			response.write "<p><b>Hooray!</b> There are no bugs in the selected list(s). To celebrate, go break something!</p>"
		End If
		%>
		</div>
		<div class="col-md-3" id="recent-actions">
			<i class="icon-spinner icon-spin icon-large"></i>
		</div>

	</div>
</div>


<!-- #include file="page_end.asp" -->
