<!-- #include file="lib/page_initialise.asp" -->
<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=UTF-8"
Response.CodePage = 65001
Response.CharSet = "UTF-8"

CheckPageAuth()

%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Buggr!</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.cerulean.min.css" rel="stylesheet">
    <link href="css/site.css" rel="stylesheet">
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Waiting+for+the+Sunrise' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="/" class="navbar-brand">Bugg<small>r</small>!</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="bugs">Bugs list<span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="bugs">
                <li><a href="bugs.asp">Open bugs (default)</a></li>
                <li class="divider"></li>
                <li><a href="bugs.asp?filter=new">New</a></li>
                <li><a href="bugs.asp?filter=open">Open</a></li>
                <li><a href="bugs.asp?filter=done">Done</a></li>
                <li><a href="bugs.asp?filter=closed">Closed</a></li>
              </ul>
            </li>
            <li><a href="#" data-toggle="modal" data-target="#addr-modal">Log a bug</a></li>
            <li><a href="profile.asp">Edit profile</a></li>
          </ul>

	      <form class="navbar-form navbar-right" role="search" method="get" action="bugs.asp">
	        <div class="form-group">
	          <input type="text" class="form-control" placeholder="Search" name="q" value="<%=request("q")%>">
	        </div>
	        <button type="submit" class="btn btn-default">Search</button>
	      </form>

        </div>
      </div>
    </div>
    
