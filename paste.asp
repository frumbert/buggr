<%@ Language=VBScript %>
<% 
Response.Expires = -1
response.buffer = false
Server.ScriptTimeout = 600
' All communication must be in UTF-8, including the response back from the request
' Session.CodePage  = 65001
%>
<!-- #include file="lib/page_initialise.asp" -->
<!-- #include file="lib/incAspUpload.asp" -->
<%

    Set filData = New FreeASPUpload ' cus we are dealing with multipart/form-data
    Call filData.Prepare() ' hack to populate form object
	Response.Write Base64.SaveBase64ToFile (filData.form("base64"), filData.form("extn"), Config.AttachmentsPath)
    set filData = Nothing

%>