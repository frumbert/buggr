<%

Dim AJAXED_LOCATION, AJAXED_LOGLEVEL, AJAXED_CONNSTRING, AJAXED_ENVIRONMENT, AJAXED_LOG_COLORIZE, AJAXED_LOGMSG_PREFIX, AJAXED_LOGSTYLE
Dim AJAXED_LOGSPATH, AJAXED_DBTYPE
	AJAXED_LOCATION = "/lib"
	AJAXED_LOGLEVEL = 0

'	AJAXED_CONNSTRING = "Driver={MySQL ODBC 5.1 Driver};Server=localhost;Database=buggr;User=buggr;Password=Bu&&r!;Option=3;"
AJAXED_CONNSTRING = "DRIVER=SQLite3 ODBC Driver;Database=" & server.mappath("/buggr.db") & ";LongNames=0;Timeout=1000;NoTXN=0;SyncPragma=NORMAL;StepAPI=0;"

Class clsConfig

	Public Shell
	Public FileSys
	Public ShellApp

	Public MyUserName
	Public MailFromAccount
	Public AttachmentsPath

	Private Sub Class_Initialize()
		Set FileSys = server.createobject("scripting.filesystemobject")
		Set Shell = server.createobject("WScript.Shell")
		Set ShellApp = Server.CreateObject("Shell.Application")

		MyUserName = Request.Cookies("Username")
		MailFromAccount = "info@coursesuite.com.au"
		AttachmentsPath = "/attachments"
		
	End Sub

	Private Sub Class_Terminate()
		set FileSys = nothing
		set Shell = nothing
		Set shellapp = nothing
	End Sub

End Class

Function NewRefId(byval clean) 
	NewRefId = NewRefId2(clean,false)
End Function

Function NewRefId2(byval clean, byval short) 
Dim s, o
	Set o = Server.CreateObject("Scriptlet.TypeLib") 
	if (short) Then
		s = Left(CStr(o.Guid), 9)
	Else
		s = Left(CStr(o.Guid), 38)
	End If
	If (clean) then
		s = replace(s, "-","")
		s = replace(s, "{","")
		s = replace(s, "}","")
	end if
	Set o = Nothing 
	NewRefId2 = s
End Function 

Function NicerDate(byval cd)
If not isdate(cd) then exit function
Dim s, d, m, y
	d = Day(cd)
	m = Month(cd)
	y = Year(cd)
	If Day(now) = d and Month(now) = m And year(now) = y Then
		s = "Today "
	ElseIf DateAdd("d",-1,now) = dateadd("d",0,cd) Then
		s = "Yesterday "
	ElseIf d = 1 Then
		s = "1st "
	ElseIf d = 2 Then
		s = "2nd "
	ElseIf d = 3 then
		s = "3rd "
	Else
		s = d & "th "
	End If
	
	If (Month(Now) = m And Year(now) = y) Then
		' skippit
	Else
		s = s & MonthName(m, true) & " "
		If Year(Now) <> y Then s = s & y & " "
	End If

	If Hour(cd) > 12 Then
		s = s & Right ("0" & Hour(cd)-12, 2) & ":"
		s = s & Right ("0" & Minute(cd), 2) & "&nbsp;p"
	Else
		s = s & Right ("0" & Hour(cd), 2) & ":"
		s = s & Right ("0" & Minute(cd), 2)
	End If
		
	NicerDate = s
	
end function

Function SendAnEmail(ByVal sFrom, ByVal sTo, ByVal sSubject, ByVal sMessage, ByVal sAttach, ByVal sUrl)
Dim Mail : Set Mail = CreateObject("CDO.Message")
	Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
	Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="mail.coursesuite.com.au"
	Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25 ' 587
	Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False 
	Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
	'Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 
	'Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="foo@coursesuite.com.au"
	'Mail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="7f8ds7d9"
	Mail.Configuration.Fields.Update
	Mail.Subject = sSubject
	Mail.From = Config.MailFromAccount
	Mail.ReplyTo = sFrom
	Mail.To = sTo
	If sAttach > "" Then
		If InStr(sAttach,":\") = 0 Then sAttach = Server.MapPath(sAttach)
		If Config.FileSys.FileExists(sAttach) Then
			Mail.AddAttachment sAttach
		End If
	End If
	Dim rex : Set rex = New RegExp
	rex.pattern = "<[^>]*>"
	If sUrl > "" Then
		Mail.CreateMHTMLBody sUrl
	ElseIf Rex.test(sMessage) Then
		Mail.HtmlBody = sMessage
	Else
		Mail.TextBody = sMessage
	End If
	Set rex = Nothing
	On Error Resume Next
	Mail.Send
	If Err Then
		SendAnEmail = False
	Else
		SendAnEmail = True
	End If
	On Error Goto 0
	Set Mail = Nothing
End Function

Function IIf(byval obj, byval yes, byval no)
	if (obj) then
		iif = yes
	else
		iif = no
	end if
end function

Sub Push2Array(byref arr, byval value)
	redim preserve arr (ubound(arr) + 1)
	arr(ubound(arr)) = value
End Sub
%>