<!-- #include file="lib/page_initialise.asp" -->

<%
Sub Unauth()
	Response.Cookies("Username") = ""
    Call Response.AddHeader("WWW-Authenticate", "Basic realm=""OurWebServerBuggerOff""")
    Response.Status = "401 Unauthorized"
    Call Response.End()
End Sub

Dim strAuth
strAuth = Request.ServerVariables("HTTP_AUTHORIZATION")

If IsNull(strAuth) Or IsEmpty(strAuth) Or strAuth = "" Then
    Call Unauth
Else 
    %>
    <html>
    <body>
    <% 
        Dim aParts, aCredentials, strType, strBase64, strPlain, strUser, strPassword
        aParts = Split(strAuth, " ")
        If aParts(0) <> "Basic" Then
            Call Unauth
        End If
        strPlain = Base64.Decode(aParts(1))
        aCredentials = Split(strPlain, ":")

	strUser = aCredentials(0)
	strPassword = aCredentials(1)
	
	db.openDefault()

	If (db.count("user", str.format("password='{1}' and name='{0}'", array(lcase(strUser),md5.hash(strPassword)))) > 0) Then
		Response.Cookies("Username") = strUser
		response.redirect ("bugs.asp")
	Else
		Response.Cookies("Username") = ""
		Response.Write "error condition " & md5.hash(strPassword)
	End If
    %>
    </body>
    </html>
    <%
End If
%>